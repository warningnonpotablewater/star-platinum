Star Platinum is an 8x8 pixel slab serif font intended for use in
artistic and embedded applications.

![Demo of the font's glyphs](screenshot.png)

## How to use

Pick a flavor of your liking [from the dist directory] and use it
accordingly.

[from the dist directory]: dist

## Build dependencies

* Python 3
* FontForge
* [Pillow]

[Pillow]: https://pillow.readthedocs.io/en/stable/

## How to rebuild

```sh
./generate.py
```

## Inspiration

* [GNU Unifont](https://unifoundry.com/unifont/)
* [IBM PC VGA font](https://commons.wikimedia.org/wiki/File:Codepage-437.png)
* [Roboto Slab](https://fonts.google.com/specimen/Roboto+Slab)

## Meaning behind the name

[Star Platinum] is a Stand from JoJo's Bizzare Adventure.

[Star Platinum]: https://jojowiki.com/Star_Platinum

## License

[Zero-Clause BSD](LICENSE).
