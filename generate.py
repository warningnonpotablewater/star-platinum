#!/usr/bin/env python3

import sys, fontforge

from PIL import Image
from pathlib import Path

GLYPH_SIZE = 8
BYTES_PER_GLYPH = int(GLYPH_SIZE ** 2 / 8)

INPUT_FILENAME = "src.png"
OUTPUT_PREFIX = "dist/star-platinum"

def main():
    atlas = read_atlas(INPUT_FILENAME)

    glyphs = split_glyphs(atlas)
    bits = split_bits(glyphs)

    generate_binary(bits)
    generate_c_header(bits)

    bdf = generate_bdf(bits)
    generate_other(bdf)

def read_atlas(filename):
    atlas = Image.open(filename).convert("1") # 1 bit color.

    if atlas.width % GLYPH_SIZE != 0 or atlas.height % GLYPH_SIZE != 0:
        print(
            f"error: Glyphs must all be {GLYPH_SIZE}x{GLYPH_SIZE} in size.",
            file=sys.stderr
        )

        sys.exit(1)

    return atlas

def split_glyphs(atlas):
    glyphs = []

    for y in range(0, atlas.height, GLYPH_SIZE):
        for x in range(0, atlas.width, GLYPH_SIZE):
            x1 = x + GLYPH_SIZE
            y1 = y + GLYPH_SIZE

            glyph = atlas.crop((x, y, x1, y1))

            glyphs.append(glyph)

    return glyphs

def split_bits(glyphs):
    bits = []

    for glyph in glyphs:
        pixels = list(glyph.getdata())
        points = 0

        for pixel in pixels:
            if pixel != 0:
                points |= 1

            points <<= 1

        points >>= 1

        rows = points.to_bytes(BYTES_PER_GLYPH, byteorder="big")

        bits.append(rows)

    return bits

def generate_binary(bits):
    contents = b"".join(bits)

    Path(f"{OUTPUT_PREFIX}.bin").write_bytes(contents)

def generate_c_header(bits):
    arrays = map(to_c_array, bits)
    glyphs = ",\n".join(arrays)

    glyph_count = len(bits)

    Path(f"{OUTPUT_PREFIX}.h").write_text(
f"""static unsigned char sp_font_data[{glyph_count}][{BYTES_PER_GLYPH}] = {{
{glyphs}
}};"""
    )

def to_c_array(bits):
    byte_list = list(map(lambda n: f"0x{n:02x}", bits))
    array = ", ".join(byte_list)

    return f"    {{{array}}}"

def generate_bdf(bits):
    data = []

    for index, glyph in enumerate(bits):
        data.append(to_bdf_data(index, glyph))

    chars = "\n".join(data)

    char_count = len(bits)
    point_size = int(GLYPH_SIZE * 0.75)

    filename = f"{OUTPUT_PREFIX}.bdf"

    Path(filename).write_text(
f"""STARTFONT 2.1
FONT StarPlatinum-Regular
SIZE {point_size} 96 96
FONTBOUNDINGBOX {GLYPH_SIZE} {GLYPH_SIZE} 0 0
STARTPROPERTIES 2
FONT_ASCENT {GLYPH_SIZE}
FONT_DESCENT 0
ENDPROPERTIES
CHARS {char_count}
{chars}
ENDFONT"""
    )

    return filename

def to_bdf_data(codepoint, bits):
    byte_list = list(map(lambda n: f"{n:02x}", bits))
    bitmap = "\n".join(byte_list)

    return f"""STARTCHAR U+{codepoint:04x}
ENCODING {codepoint}
SWIDTH 1000 0
DWIDTH {GLYPH_SIZE} 0
BBX {GLYPH_SIZE} {GLYPH_SIZE} 0 0
BITMAP
{bitmap}
ENDCHAR"""

def generate_other(bdf):
    font = fontforge.open(bdf)

    font.appendSFNTName("English (US)", "Copyright", " ")
    font.appendSFNTName("English (US)", "UniqueID", " ")
    font.appendSFNTName("English (US)", "Version", " ")

    font.generate(f"{OUTPUT_PREFIX}.otb")

if __name__ == "__main__":
    main()
